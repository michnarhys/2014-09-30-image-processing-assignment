#import necessary libraries
import numpy as np
import matplotlib.pyplot as plt
import scipy.ndimage as ndimage
import scipy.misc as misc
import os
from images2gif import writeGif
from PIL import Image
from skimage.feature import corner_harris
#image pixels and variables
pixels = 256
x = range(pixels)
y = range(pixels)
triangle_image = np.zeros((pixels, pixels))

def point_inside_polygon(x,y,poly): # fills the first initial triangle vertices
    n = len(poly)
    inside = False
    p1x,p1y = poly[0]
    for i in range(n+1):
        p2x,p2y = poly[i % n]
        if y > min(p1y,p2y):
            if y <= max(p1y,p2y):
                if x <= max(p1x,p2x):
                    if p1y != p2y:
                        xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                    if p1x == p2x or x <= xinters:
                        inside = not inside
        p1x,p1y = p2x,p2y
    return inside

def generatetriangle(): #creates first filled in triangle image
	#create first three vertices
	vertices = (128,90), (100, 150), (168, 150)
	#fill the triangle
	for ii in range(pixels):
		for jj in range(pixels):
			triangle_image[jj][ii] = point_inside_polygon(x[ii],y[jj],vertices)
	return triangle_image

def spin_save(previousimage, total_rotations): # rotate image
		for i in range(total_rotations):
			previousimage = ndimage.rotate(previousimage, 2, reshape = False)
			filename = 'triangle_{0}'.format(i)
			misc.imsave('{0}.png'.format(filename), previousimage)
		return previousimage

def animate():
	file_names = sorted((fn for fn in os.listdir('.') if fn.endswith('.png')))
	images = [Image.open(fn) for fn in file_names]
	filename = "Triangle_gif.GIF"
	writeGif(filename, images, duration=0.05)

def GetBinaryVertexImage(image):
	image_mask = (image > 6*image.mean()).astype(np.float)
	image_opening = ndimage.binary_opening(image_mask)
	dilated_image = ndimage.binary_dilation(image_mask - image_opening)
	plt.imshow(dilated_image, cmap=plt.cm.gray)
	plt.axis('off')
	plt.title('Binary_Vertex')
	plt.savefig('Binary_Vertex.png')

def GetHarrisVertexImage(image, kappa = 0.5, windowsize = 9, windowspace = 1, sigma = 2):
	Ix = ndimage.sobel(image,axis=0)
	Iy = ndimage.sobel(image, axis = 1)
	N = windowsize
	gw = np.zeros((N,N)) #N should be odd
	center = (N-1)/2
	gw[center,center] = 1
	gw = ndimage.gaussian_filter(gw, sigma)
	harrisimage = np.zeros(image.shape)
	for ii in range(center, image.shape[0] - center, windowspace):
		for jj in range(center, image.shape[1] - center, windowspace):
				Ix_window = Ix[ii-center : ii - center + N, jj - center : jj - center + N]
				Iy_window = Iy[ii-center : ii - center + N, jj - center : jj - center + N]
				M = tensor(Ix_window,Iy_window, gw)
				harrisimage[ii,jj] = np.linalg.det(M) - kappa * np.trace(M)**2
	plt.imshow(harrisimage*-1, cmap=plt.cm.gray)
	plt.axis('off')
	plt.title('My_Harris_Vertex')
	plt.savefig('My_Harris_Vertex.png')

def tensor(Ix_window, Iy_window, gw):
	Ix_2 = np.multiply(Ix_window, Ix_window)
	Ix_2_avg = np.sum(np.multiply(Ix_2,gw))
	Iy_2 = np.multiply(Iy_window, Iy_window)
	Iy_2_avg = np.sum(np.multiply(Iy_2, gw))
	IxIy = np.multiply(Ix_window, Iy_window)
	IxIy_avg = np.sum(np.multiply(IxIy, gw))
	M = np.zeros((2,2))
	M[0][0] = Ix_2_avg
	M[0][1] = IxIy_avg
	M[1][0] = IxIy_avg
	M[1][1] = Iy_2_avg
	return M

def timematrix(Ix_window, Iy_window, It_window):
	b = np.zeros((2,1))
	b[0,0] = -np.sum(np.multiply(Ix_window, It_window))
	b[1,0] = -np.sum(np.multiply(Iy_window, It_window))
	return b#called in lucas kanade function

# def LucasKanadeFlowFields(image1, image2, dt, windowsize = 9, windowspace = 2, sigma = 2):
# 	Ix = ndimage.sobel(image1, axis = 0)
# 	Iy = ndimage.sobel(image1, axis = 1)
# 	It = (image1-image2)/dt
# 	N = windowsize
# 	gw = np.zeros((N,N)) #N should be odd
# 	center = (N-1)/2
# 	gw[center,center] = 1
# 	gw = ndimage.gaussian_filter(gw, sigma)
# 	lucasimage = np.zeros(image1.shape)
# 	for ii in range(center, image1.shape[0] - center, windowspace):
# 		for jj in range(center, image1.shape[1] - center, windowspace):
# 				Ix_window = Ix[ii-center : ii - center + N, jj - center : jj - center + N]
# 				Iy_window = Iy[ii-center : ii - center + N, jj - center : jj - center + N]
# 				It_window = It[ii-center : ii - center + N, jj - center : jj - center + N]
# 				M = tensor(Ix_window,Iy_window, gw)
# 				b = timematrix(Ix_window, Iy_window, It_window)
# 				lucasimage[ii,jj] = (Ix_window, Iy_window, It_window)((np.linalg.inv(M) *b))
# 	plt.imshow(lucasimage, cmap=plt.cm.gray)
# 	plt.axis('off')
# 	plt.show()

def lucas_kanade_np(im1, im2, win=2): #code for lucas kanade on stockoverflow
    assert im1.shape == im2.shape
    I_x = np.zeros(im1.shape)
    I_y = np.zeros(im1.shape)
    I_t = np.zeros(im1.shape)
    I_x[1:-1, 1:-1] = (im1[1:-1, 2:] - im1[1:-1, :-2]) / 2
    I_y[1:-1, 1:-1] = (im1[2:, 1:-1] - im1[:-2, 1:-1]) / 2
    I_t[1:-1, 1:-1] = im1[1:-1, 1:-1] - im2[1:-1, 1:-1]
    params = np.zeros(im1.shape + (5,)) #Ix2, Iy2, Ixy, Ixt, Iyt
    params[..., 0] = I_x * I_x # I_x2
    params[..., 1] = I_y * I_y # I_y2
    params[..., 2] = I_x * I_y # I_xy
    params[..., 3] = I_x * I_t # I_xt
    params[..., 4] = I_y * I_t # I_yt
    del I_x, I_y, I_t
    cum_params = np.cumsum(np.cumsum(params, axis=0), axis=1)
    del params
    win_params = (cum_params[2 * win + 1:, 2 * win + 1:] -
                  cum_params[2 * win + 1:, :-1 - 2 * win] -
                  cum_params[:-1 - 2 * win, 2 * win + 1:] +
                  cum_params[:-1 - 2 * win, :-1 - 2 * win])
    del cum_params
    op_flow = np.zeros(im1.shape + (2,))
    det = win_params[...,0] * win_params[..., 1] - win_params[..., 2] **2
    op_flow_x = np.where(det != 0,
                         (win_params[..., 1] * win_params[..., 3] -
                          win_params[..., 2] * win_params[..., 4]) / det,
                         0)
    op_flow_y = np.where(det != 0,
                         (win_params[..., 0] * win_params[..., 4] -
                          win_params[..., 2] * win_params[..., 3]) / det,
                         0)
    op_flow[win + 1: -1 - win, win + 1: -1 - win, 0] = op_flow_x[:-1, :-1]
    op_flow[win + 1: -1 - win, win + 1: -1 - win, 1] = op_flow_y[:-1, :-1]
    return op_flow

def main():
	#get first triangle image
	tri_filled = generatetriangle()
	#rotate the image and save new image 100 times
	vertex_detection_image = spin_save(tri_filled, 100)
	#get image png files and animate to create movie
	animate()
	#built in harris function for comparison
	python_harris = corner_harris(vertex_detection_image)
	plt.imshow(python_harris, cmap=plt.cm.gray)
	plt.axis('off')
	plt.title('Harris_Vertex')
	plt.savefig('Harris_Vertex.png')
	#vetex detection of a triangle
	GetBinaryVertexImage(vertex_detection_image)
	GetHarrisVertexImage(vertex_detection_image)
	#generate images of the velocity field, plot x(t) and y(t) for each corner
	Lucas_image_1 = corner_harris(spin_save(tri_filled, 58))
	Lucas_image_2 = corner_harris(spin_save(tri_filled, 57))
	# LucasKanadeFlowFields(Lucas_image_1, Lucas_image_2, 0.01)    - failed function
	lucas_k = lucas_kanade_np(Lucas_image_1, Lucas_image_2, win=9)
	plt.imshow(lucas_k, cmap=plt.cm.gray)
	plt.axis('off')
	plt.title('Lucas_Image')
	plt.savefig('Lucas_Image.png')

#invoke main function to run program
if __name__ == '__main__':
	main()